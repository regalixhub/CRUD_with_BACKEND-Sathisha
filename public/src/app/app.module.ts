import { TaskServiceService } from './task-service.service';
import { FormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { ListTasksComponent } from './list-tasks/list-tasks.component';
import { TaskFormComponent } from './task-form/task-form.component';
import { EditTaskComponent } from './edit-task/edit-task.component';
import { HttpClientModule } from '@angular/common/http';
import { TaskDetailsComponent } from './task-details/task-details.component';
import { BsDatepickerModule } from 'ngx-bootstrap/datepicker';
import { ListTaskItemsComponent } from './list-task-items/list-task-items.component';

@NgModule({
  declarations: [
    AppComponent,
    ListTasksComponent,
    TaskFormComponent,
    EditTaskComponent,
    TaskDetailsComponent,
    ListTaskItemsComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule,
    BsDatepickerModule.forRoot()
  ],
  providers: [TaskServiceService],
  bootstrap: [AppComponent]
})
export class AppModule { }
