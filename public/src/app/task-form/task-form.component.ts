import { Component, OnInit } from '@angular/core';
import { TaskServiceService } from '../task-service.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-task-form',
  templateUrl: './task-form.component.html',
  styleUrls: ['./task-form.component.css']
})
export class TaskFormComponent {

  constructor(private taskService: TaskServiceService,
              private _router: Router) { }

  onClickSubmit(data) {
    this.taskService.createTask(data).subscribe( () => {
      this._router.navigate(['/list']);
    });
  }

}
