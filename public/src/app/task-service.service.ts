import { Task } from './task';
import { Injectable } from '@angular/core';
import { HttpHeaders, HttpClient } from '@angular/common/http';
import { Observable, of } from 'rxjs';
import { catchError } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})

export class TaskServiceService {

  private baseUrl = 'http://localhost:8080/api/task';
  private headers = new HttpHeaders({ 'Content-type': 'application/json' });
  private options = { headers: this.headers };

  constructor(private http: HttpClient) { }

  getTasks(): Observable<Task[]> {
    return this.http.get<Task[]>(this.baseUrl + '/', this.options).pipe(
      catchError(this.handleError('getTasks', []))
    );
  }

  getTaskById(id: number): Observable<Task> {
    return this.http.get<Task>(this.baseUrl + '/' + id, this.options).pipe(
      catchError(this.handleError('getTask', new Task(0, '', new Date(), '')))
    );
  }

  createTask(task: Task): Observable<Task> {
    return this.http.post<Task>(this.baseUrl + '/', JSON.stringify(task), this.options).pipe(
      catchError(this.handleError('createTask',  new Task(0, '', new Date(), '')))
    );
  }

  updateTask(task: Task): Observable<Task> {
    return this.http.put<Task>(this.baseUrl + '/' + task.id , JSON.stringify(task), this.options).pipe(
      catchError(this.handleError('updateTask',  new Task(0, '', new Date(), '')))
    );
  }

  deleteTask(id: number) {
    return this.http.delete(this.baseUrl + '/' + id, this.options).pipe(
      catchError(this.handleError('deleteTask'))
    );
  }

  private handleError<T>(operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {
      console.error(error);
      return of(result as T);
    };
  }

}
