import { Component, OnInit } from '@angular/core';
import { TaskServiceService } from '../task-service.service';
import { Task } from '../task';

@Component({
  selector: 'app-list-tasks',
  templateUrl: './list-tasks.component.html',
  styleUrls: ['./list-tasks.component.css']
})
export class ListTasksComponent implements OnInit {

  public tasks: Task[] = [];
  constructor(private _service: TaskServiceService) { }

  ngOnInit() {
    this._service.getTasks().subscribe(data => {
    this.tasks = data;
      this.tasks.sort(function (a, b) {
        return a.id - b.id;
      });
    });
  }

}
