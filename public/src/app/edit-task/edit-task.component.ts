import { TaskServiceService } from './../task-service.service';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router, ParamMap } from '@angular/router';
import { Task } from '../task';

@Component({
  selector: 'app-edit-task',
  templateUrl: './edit-task.component.html',
  styleUrls: ['./edit-task.component.css']
})
export class EditTaskComponent implements OnInit {

  public task: Task;

  constructor(private _service: TaskServiceService,
              private _route: ActivatedRoute,
              private _router: Router) { }

  ngOnInit() {
    this._route.paramMap.subscribe((params: ParamMap) => {
      const index = parseInt(params.get('id'), 0);
      this._service.getTaskById(index)
        .subscribe(_task => this.task = _task);
    });
  }
  update() {
    this._service.updateTask(this.task).subscribe(() =>
      this._router.navigate(['/list'])
    );
  }

}
