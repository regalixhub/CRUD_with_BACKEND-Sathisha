import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ListTaskItemsComponent } from './list-task-items.component';

describe('ListTaskItemsComponent', () => {
  let component: ListTaskItemsComponent;
  let fixture: ComponentFixture<ListTaskItemsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListTaskItemsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListTaskItemsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
