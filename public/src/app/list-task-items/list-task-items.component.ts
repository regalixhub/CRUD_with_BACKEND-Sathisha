import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { TaskServiceService } from '../task-service.service';
import { Router, ActivatedRoute } from '@angular/router';
import { Task } from '../task';

@Component({
  selector: 'app-list-task-items',
  templateUrl: './list-task-items.component.html',
  styleUrls: ['./list-task-items.component.css']
})
export class ListTaskItemsComponent {


  @Input() public tasks: Task[] = [];
  @Output() public childEvent = new EventEmitter();

  constructor(private _service: TaskServiceService,
              private _router: Router,
              private _route: ActivatedRoute) { }


  detail(id: number) {
    this._router.navigate(['/details', id]);
  }

  edit(id: number) {
    this._router.navigate(['/edit', id]);
  }

  delete(id: number, i: number) {
    this._service.deleteTask(id).subscribe();
    this.tasks.splice(i, 1);
  }
}
