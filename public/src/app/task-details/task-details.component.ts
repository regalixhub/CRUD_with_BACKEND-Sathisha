import { TaskServiceService } from './../task-service.service';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router, ParamMap } from '@angular/router';
import { Task } from '../task';


@Component({
  selector: 'app-task-details',
  templateUrl: './task-details.component.html',
  styleUrls: ['./task-details.component.css']
})
export class TaskDetailsComponent implements OnInit {

  public task: Task;

  constructor(private _service: TaskServiceService,
              private _route: ActivatedRoute,
              private _router: Router) { }

  ngOnInit() {
    this._route.paramMap.subscribe((params: ParamMap) => {
      const index = parseInt(params.get('id'), 0);
      this._service.getTaskById(index)
        .subscribe(_task => this.task = _task);
    });
  }

}
