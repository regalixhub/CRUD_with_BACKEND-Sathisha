import { BrowserModule } from '@angular/platform-browser';
import { ListTasksComponent } from './list-tasks/list-tasks.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { EditTaskComponent } from './edit-task/edit-task.component';
import { TaskFormComponent } from './task-form/task-form.component';
import { TaskDetailsComponent } from './task-details/task-details.component';

const routes: Routes = [
  {path : '', redirectTo: '/list', pathMatch : 'full'},
  {path: 'list', component: ListTasksComponent},
  {path: 'edit/:id', component: EditTaskComponent},
  {path: 'add', component: TaskFormComponent},
  {path: 'details/:id', component: TaskDetailsComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
